// Client ID and API key from the Developer Console
var CLIENT_ID = '774465163803-0k95jdr1mimtjliudcdtfkn6tg36a5d5.apps.googleusercontent.com';
var API_KEY = 'AIzaSyD5FpJhG0a22qBEAc8bOHbTwvnzFiHv9t8';
// Array of API discovery doc URLs for APIs used by the quickstart
var DISCOVERY_DOCS = 
["https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest",
"https://www.googleapis.com/discovery/v1/apis/people/v1/rest"];
// Authorization scopes required by the API. If using multiple scopes,
// separated them with spaces.
var SCOPES = 'https://www.googleapis.com/auth/youtube '
            +'https://www.googleapis.com/auth/contacts.readonly';
var authorizeButton = document.getElementById('authorize-button');
var signoutButton = document.getElementById('signout-button');
/**
 *  On load, called to load the auth2 library and API client library.
 */
function handleClientLoad() {
gapi.load('client:auth2', initClient);
}
/**
 *  Initializes the API client library and sets up sign-in state
 *  listeners.
 */
function initClient() {
gapi.client.init({
   apiKey: API_KEY,
    discoveryDocs: DISCOVERY_DOCS,
    clientId: CLIENT_ID,
    scope: SCOPES
}).then(function () {
    // Listen for sign-in state changes.
    gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
    // Handle the initial sign-in state.
    updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
    authorizeButton.onclick = handleAuthClick;
    signoutButton.onclick = handleSignoutClick;
});
}
/**
 *  Called when the signed in status changes, to update the UI
 *  appropriately. After a sign-in, the API is called.
 */
function updateSigninStatus(isSignedIn) {
if (isSignedIn) {
    authorizeButton.style.display = 'none';
    signoutButton.style.display = 'block';
    //getChannel();

    listConnectionNames();
    handleAPILoaded();
} else {
    authorizeButton.style.display = 'block';
    signoutButton.style.display = 'none';
}
}
/**
 *  Sign in the user upon button click.
 */
function handleAuthClick(event) {
gapi.auth2.getAuthInstance().signIn();
}
/**
 *  Sign out the user upon button click.
 */
function handleSignoutClick(event) {
gapi.auth2.getAuthInstance().signOut();
}
/**
 * Append text to a pre element in the body, adding the given message
 * to a text node in that element. Used to display info from API response.
 *
 * @param {string} message Text to be placed in pre element.
 */
function appendPre(message) {
var pre = document.getElementById('content');
var textContent = document.createTextNode(message + '\n');
pre.appendChild(textContent);
}

function handleAPILoaded() {
    $('#search-button').attr('disabled', false);
}

function tplawesome(e,t)
{
    res=e;
    for(var n=0;n<t.length;n++){

        res=res.replace(/\{\{(.*?)\}\}/g,function(e,r){
            return t[n][r]
        })
    }
    return res
}

  
      function listConnectionNames() {
        var contenedor = document.getElementById('content');
        var h = document.createElement('h');
        var con= document.getElementById('contactos');

        gapi.client.people.people.connections.list({
           'resourceName': 'people/me',
          // 'pageSize': 15,
           'personFields': 'names',
         }).then(function(response) {
           var connections = response.result.connections;
             appendPre('Lista de Contactos:');

if(typeof(connections)!="undefined"){
           if (connections.length > 0) {
             for (i = 0; i < connections.length; i++) {
               var person = connections[i];
               
               if (person.names && person.names.length > 0) {
                // appendPre(person.names[0].displayName)
                 var a=person.names[0].displayName;
                 var opcion=document.createElement("option");
                 opcion.value=a;
                 opcion.innerHTML=a;
                 con.appendChild(opcion);
                 //console.log(a);

                } else {
                 //appendPre("Sin Nombre.");
               }
                //appendPre("\n");
             }
           } else {
             appendPre('parece que no tienes contactos de people');
           }
         }else{
          appendPre('parece que no tienes contactos en tu cuenta People :(');
         }
         });
      }





window.onload =principal;
var array=[];
var indice=0;

function principal() {

      var lista_videos=new Array();
      document.getElementById("b1").addEventListener('click', function (){indice=0;procesar()});
 // document.getElementById("buttons").addEventListener('click', function (){indice=0;procesar()});
 

     
         function main(){
                              document.getElementById("contenido").innerHTML="";
                              document.getElementById("pagi").innerHTML="";

         for (var i = 0; i < array.length; i++) {
                           if(indice==i && array.length>0){
                                 for (var j = 0; j < array[i].length; j++) {
                                       //console.log(array[i][j].length);

                                       agregar_video(array[i][j]);
                                 }
                           }else{console.log("nada");}
                     }

                     var siguiente=document.createElement('button');
                     var atras=document.createElement('button');

                     siguiente.innerHTML="siguiente";
                     atras.innerHTML="atras";
                     siguiente.id="sig";
                     atras.id="atras";
                     var selector = document.getElementById('contenido');

                     var pagi = document.getElementById('pagi');
                    
                     pagi.appendChild(atras);
                     pagi.appendChild(siguiente);
                     document.getElementById("atras").addEventListener('click',function(){
                             if (indice>0){indice--;}
                           main();});
                     document.getElementById("sig").addEventListener('click',function(){
                           if (indice<5){indice++;}
                           main();});

               }

               function procesar(){ 
                var bus = document.getElementById('contactos').value;
                var val=document.getElementById("buscarfield").value;
                var cantidad= document.getElementById("cantidad").value;
                 // console.log(cantidad);
                      iniciar();
                      if (cantidad>0 && val!=="") {
                         requisitos={
                      part: "snippet",
                      type: "video",
                      q: val+" "+bus,
                      maxResults: cantidad,
                     
                      publishedAfter: "2010-01-01T00:00:00Z"
                  }
                }
                else{
                  requisitos={
                      part: "snippet",
                      type: "video",
                      q: val+" "+bus,
                      maxResults: 5,
                    
                      publishedAfter: "2010-01-01T00:00:00Z"
                  }
                }
                     

                        iniciar();
                          document.getElementById("contenido").innerHTML="";
                           var request = gapi.client.youtube.search.list(requisitos);
                           request.execute(function(response)
                          {
                              var resultado = response.result.items;
                              //console.log("imprimiendo resultado");
                              //console.log(resultado);
                              var elem=[];
                              var inc=0;
                              array=[];
                              console.log(cantidad+"cantidad que es numero de videos");

                              
                       
                            
                              
                              if(cantidad>10){
                                  var aux=10;
                          // var valor= (Math.trunc(cantidad/10))+1;
         //  console.log("este es el 'valor'  de un Math.trunc(cantidad/10): "+valor);
                              //if(valor>=0){
                                //valor=valor+1;
                                
                               // var r=resultado.length;
                                    //console.log("este es el valor de r=resltado.length: "+r);
                               
                         //     for(var a=0 ;a <valor;a++){
                              for (var i = 0; i < resultado.length; i++) {
                                aux=aux-resultado.length;
                                                if(aux>=0 && aux <=10){
                                                  if(i%aux===0 && i!=0  ){

                                                   array.push(elem)
                                                   elem=new Array();
                                                   elem.push(resultado[i]);
                                                }else{
                                                   elem.push(resultado[i]);

                                                }
                                                }else{
                                                  if(i%10===0 && i!=0  ){

                                                   array.push(elem)
                                                   elem=new Array();
                                                   elem.push(resultado[i]);
                                                }else{
                                                   elem.push(resultado[i]);

                                                }
                                                aux=aux+10;
                                                }
                                                
                                        //  }
                                     //     r=r-10;
                                         // console.log(r);
//}
                                      }
                                        }else{
                                         for (var i = 0; i < resultado.length; i++) {
                                                if(i%cantidad===0 && i!=0){
                                                   array.push(elem)
                                                   elem=new Array();
                                                   elem.push(resultado[i]);
                                                }else{
                                                   elem.push(resultado[i]);

                                                }
                                          }

                                        }
                                       array.push(elem);
                                       main();
                                    });
                              }

               function coordenadas(ids, vid){
                  //XcjaD5Fw2Mg
                  //7a7AjyjURhM
                  var video;
                  var corde={};
                  var altitude="No asignado";
                  var latitude="No asignado";
                  var longitude="No asignado";
                  var request_video = gapi.client.youtube.videos.list({
                              part: 'recordingDetails',
                              id: ids
                           });
                           request_video.execute(function(response)
                           {

                              var resultado_coord = response.result.items;
                              //console.log("imprimiendo response.result");
                              //console.log(responseultado_coord);
                             // console.log("imprimiendo recordingDetails.location");
                           //  console.log(resultado_coord[0].recordingDetails);
                              if (resultado_coord[0].hasOwnProperty('recordingDetails')){

                                if (resultado_coord[0].recordingDetails.hasOwnProperty('location')){
                                  if(resultado_coord[0].recordingDetails.location.hasOwnProperty('latitude')){
                                  latitude=resultado_coord[0].recordingDetails.location.latitude;
                                  }
                                   if(resultado_coord[0].recordingDetails.location.hasOwnProperty('longitude')){
                                  longitude=resultado_coord[0].recordingDetails.location.longitude;
                                  }
                                   if(resultado_coord[0].recordingDetails.location.hasOwnProperty('altitude')){
                                  altitude=resultado_coord[0].recordingDetails.location.altitude;
                                  }
                                  video=vid;
                                  video.height="100";
                                  video.width=1280/10;
                                }
                               // console.log(latitude);
                             //   console.log(longitude);
                                corde.lat=latitude;
                                corde.lng=longitude;
//console.log(corde);
                               // initMap(latitude,longitude);
marcar(corde, video);

                              
                         }});
return corde;
}

   function agregar_video( resultado){
    initMap();
var cantidad= document.getElementById("cantidad").value;

            
                var fecha=new Date(resultado.snippet.publishedAt);
                var subida=fecha.getUTCDate()+"/"+(fecha.getUTCMonth()+1)+"/"+fecha.getFullYear();
                console.log(fecha.getUTCDate()+"/"+(fecha.getUTCMonth()+1)+"/"+fecha.getFullYear());
               var selector = document.getElementById('contenido');
              

               var iframe = document.createElement('iframe');
               if(cantidad<=10){
                if(cantidad>0){
               iframe.width=1280/cantidad;
             
               iframe.height="100";
             }else{

               iframe.width=1280/5;
             
               iframe.height="100";
             }
             }else{
              iframe.width=1280/10;
             
               iframe.height="100px";
             }
               iframe.setAttribute('src','https://www.youtube.com/embed/'+resultado.id.videoId);
           
               coordenadas(resultado.id.videoId, iframe);
       
               selector.appendChild(iframe);
}
}
            function iniciar() {

 gapi.client.setApiKey("AIzaSyD5FpJhG0a22qBEAc8bOHbTwvnzFiHv9t8");
 gapi.client.load("youtube", "v3", function() { 

 });


}

var map;
function initMap(){
  map= new google.maps.Map(document.getElementById('map'),{
    zoom: 2,
    center: new google.maps.LatLng(30,11)
  }); 
}

function marcar(corde, vid){
var most=vid;

 var infowindow = new google.maps.InfoWindow({
    content: most
  });


  var marker = new google.maps.Marker({
    position: corde,
    map: map
  })

  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });
}

function marcarTwitter(la, lo, te){
  var a =document.getElementById("buscarfield").value;
var posi={lat:la , lng: lo}
var icono='punto.png'
 var infowindow = new google.maps.InfoWindow({
    content: "#"+a+" "+te
  });


  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(la,lo),
    map: map,
    icon: icono
  })

  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });
}

//Empezamos con twitter :V 
